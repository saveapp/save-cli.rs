use save::api;
use save::Config;
use save::SaveError;

pub fn remove(id: u64) -> Result<reqwest::Response, SaveError> {
    let config = Config::from_file(None)?;
    Ok(api::remove_link(id, &config)?)
}
