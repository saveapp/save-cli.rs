use save::api;
use save::util;
use save::SaveError;
use save::{Config, Link};

pub fn list(filter_str: Option<&str>) -> Result<Vec<Link>, SaveError> {
    let config = Config::from_file(None)?;
    let all_links = api::get_all_links(&config)?;
    Ok(util::filter(filter_str, all_links))
}
