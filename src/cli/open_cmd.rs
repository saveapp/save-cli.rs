use save::api;
use save::util;
use save::Config;
use save::Link;
use save::SaveError;

pub fn open(ids: Vec<u64>) -> Result<u16, SaveError> {
    let config = Config::from_file(None)?;

    let links: Vec<Result<Link, SaveError>> =
        ids.iter().map(|id| api::get_link(*id, &config)).collect();
    let mut opened: u16 = 0;
    for link in links {
        util::open_link(&link?, &config)?;
        opened += 1;
    }

    Ok(opened)
}
