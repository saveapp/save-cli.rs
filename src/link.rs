use std::fmt;

use serde::Deserialize;

#[derive(Deserialize, Debug, Clone)]
pub struct Link {
    id: u64,
    url: String,
    annotation: String,
    created_on: String,
}

impl Link {
    pub fn new(id: u64, url: &str, annotation: &str, created_on: &str) -> Self {
        let url = String::from(url);
        let annotation = String::from(annotation);
        let created_on = String::from(created_on);
        Link {
            id,
            url,
            annotation,
            created_on,
        }
    }

    pub fn id(&self) -> u64 {
        self.id
    }

    pub fn url(&self) -> &str {
        &self.url
    }

    pub fn annotation(&self) -> &str {
        &self.annotation
    }

    pub fn created_on(&self) -> &str {
        &self.created_on
    }
}

impl fmt::Display for Link {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut s;
        if f.alternate() {
            // format to make it readable in a list
            s = format!("{}: {}", self.id(), self.url());
            if self.annotation() != "" {
                s = format!("{}\n  {}", s, self.annotation());
            }
            s = format!("{}\n  {}", s, self.created_on());
        } else {
            // format to make it readable on its own
            s = format!("{}", self.url);
            if self.annotation != "" {
                s = format!("{} - {}", s, self.annotation);
            }
            s = format!("{} ({})", s, self.created_on);
        }
        write!(f, "{}", s)
    }
}

impl std::cmp::PartialEq for Link {
    fn eq(&self, other: &Link) -> bool {
        let id = self.id == other.id;
        let url = self.url == other.url;
        let annotation = self.annotation == other.annotation;

        return id && url && annotation;
    }
}
