use std::error::Error as StdError;
use std::fmt;
use std::io::Error as IOError;
use std::io::ErrorKind as IOErrorKind;
use std::process::Command;

use crate::Config;
use crate::Link;

#[derive(Debug, PartialEq)]
pub enum SaveError {
    ConfigNotFound(String),
    ConfigMalformatted(String),
    ConfigMissing(String),
    ApiError(String),
    Other(String),
}
impl fmt::Display for SaveError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            SaveError::ConfigNotFound(desc) => write!(f, "Can't find config file: {}", desc),
            SaveError::ConfigMalformatted(desc) => {
                write!(f, "Configuration contains an error: {}", desc)
            }
            SaveError::ConfigMissing(desc) => {
                write!(f, "Mandatory value not found in config: {}", desc)
            }
            SaveError::ApiError(desc) => write!(f, "Error communicating with the server: {}", desc),
            SaveError::Other(desc) => write!(f, "Error: {}", desc),
        }
    }
}
impl StdError for SaveError {
    fn description(&self) -> &str {
        match self {
            SaveError::ConfigNotFound(desc) => desc,
            SaveError::ConfigMalformatted(desc) => desc,
            SaveError::ConfigMissing(desc) => desc,
            SaveError::ApiError(desc) => desc,
            SaveError::Other(desc) => desc,
        }
    }
}
impl From<IOError> for SaveError {
    fn from(e: IOError) -> Self {
        match e.kind() {
            IOErrorKind::NotFound => SaveError::ConfigNotFound(String::from(e.to_string())),
            _ => SaveError::Other(String::from(e.to_string())),
        }
    }
}
impl From<std::env::VarError> for SaveError {
    fn from(e: std::env::VarError) -> Self {
        SaveError::ConfigMissing(String::from(e.to_string()))
    }
}
impl From<serde_yaml::Error> for SaveError {
    fn from(e: serde_yaml::Error) -> Self {
        SaveError::ConfigMalformatted(String::from(e.to_string()))
    }
}

impl From<reqwest::Error> for SaveError {
    fn from(e: reqwest::Error) -> Self {
        SaveError::ApiError(String::from(e.to_string()))
    }
}

pub fn url(conf: &Config, endpoint: &str) -> String {
    let url = format!(
        "{api_url}/{endpoint}",
        api_url = conf.api_url(),
        endpoint = endpoint
    );
    return url;
}

pub fn filter(string: Option<&str>, links: Vec<Link>) -> Vec<Link> {
    match string {
        Some("") => return links.into_iter().filter(|l| l.annotation() == "").collect(),
        Some(fs) => {
            return links
                .into_iter()
                .filter(|l| l.annotation().contains(fs))
                .collect()
        }
        None => return links,
    }
}

pub fn open_link(link: &Link, config: &Config) -> Result<(), SaveError> {
    match config.open_command() {
        Some(oc) => {
            let mut command = Command::new(oc.command());
            if let Some(args) = oc.args() {
                let mut command_args: Vec<String> = args;
                command_args.push(String::from(link.url()));
                command.args(command_args);
            };
            command.output()?;
        }
        None => {
            webbrowser::open(link.url())?;
        }
    }
    Ok(())
}

#[cfg(test)]
mod test_url {
    use super::*;

    #[test]
    fn should_create_correct_url() {
        let config: Config = Config::new(
            "https://save.example.com",
            "tester",
            Some("sup3rs3cure"),
            None,
        );
        let endpoint = "api/links";
        let result = url(&config, endpoint);

        assert_eq!(result, "https://save.example.com/api/links");
    }
}

#[cfg(test)]
mod test_filter {
    use super::*;

    #[test]
    fn should_filter_links_matching_annotation() {
        let links: Vec<Link> = vec![
            Link::new(1, "https://example.com", "test", "2018-12-30"),
            Link::new(2, "https://example.com/nope", "", "2018-12-30"),
            Link::new(3, "https://example.com/yup", "another test", "2018-12-30"),
        ];
        let annotation_filter = Some("test");
        let result = filter(annotation_filter, links);

        assert_eq!(result.len(), 2);
        assert_eq!(result[0].url(), "https://example.com");
        assert_eq!(result[1].url(), "https://example.com/yup");
    }

    #[test]
    fn should_not_filter_anything_without_filter() {
        let link1 = Link::new(1, "https://example.com", "test", "2018-12-30");
        let link2 = Link::new(2, "https://example.com/2", "", "2018-12-30");
        let link3 = Link::new(3, "https://example.com/yup", "another test", "2018-12-30");
        let links: Vec<Link> = vec![link1.clone(), link2.clone(), link3.clone()];
        let annotation_filter = None;
        let result = filter(annotation_filter, links);

        assert_eq!(result, vec![link1, link2, link3]);
    }

    #[test]
    fn should_not_filter_empty_annotations_on_empty_filter() {
        let link1 = Link::new(1, "https://example.com", "test", "2018-12-30");
        let link2 = Link::new(2, "https://example.com/2", "", "2018-12-30");
        let link3 = Link::new(3, "https://example.com/yup", "another test", "2018-12-30");
        let links: Vec<Link> = vec![link1, link2.clone(), link3];
        let annotation_filter = Some("");
        let result = filter(annotation_filter, links);

        assert_eq!(result, vec![link2]);
    }
}
