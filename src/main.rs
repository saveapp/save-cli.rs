use std::str::FromStr;

use clap::{crate_name, crate_version};
use clap::{App, Arg, SubCommand};

mod cli;

fn main() {
    const APP_NAME: &str = crate_name!();
    const APP_VERSION: &str = crate_version!();
    const ABOUT: &str = "Save links for reading later and read saved links";

    let args = App::new(APP_NAME)
        .version(APP_VERSION)
        .about(ABOUT)
        .subcommand(
            SubCommand::with_name("list")
                .about("List your saved links")
                .arg(
                    Arg::with_name("filter")
                        .help("Only show links with the filtered string in its annotation")
                        .value_name("FILTER")
                        .short("f")
                        .long("filter"),
                )
                .arg(
                    Arg::with_name("print")
                        .help("Print to stdout instead of displaying a pager")
                        .short("p")
                        .long("print"),
                ),
        )
        .subcommand(
            SubCommand::with_name("open")
                .about("Open a saved link in a webbrowser, or with the specified open_command in your config")
                .arg(
                    Arg::with_name("id")
                        .help("The link ID to open")
                        .value_name("LINK_ID")
                        .required(true)
                        .multiple(true)
                        .value_delimiter(","),
                ),
        )
        .subcommand(
            SubCommand::with_name("save")
                .about("Save a link")
                .arg(
                    Arg::with_name("url")
                        .help("The link to save")
                        .value_name("URL")
                        .required(true),
                )
                .arg(
                    Arg::with_name("annotation")
                        .help("An optional annotation for the link")
                        .value_name("ANNOTATION")
                        .required(false),
                ),
        )
        .subcommand(
            SubCommand::with_name("remove")
                .about("Remove a previously saved link")
                .arg(
                    Arg::with_name("id")
                        .help("The link to remove")
                        .value_name("LINK_ID")
                        .required(true),
                ),
        )
        .get_matches();

    if let Some(cmd) = args.subcommand_matches("list") {
        let filter: Option<&str> = cmd.value_of("filter");
        let print: bool = cmd.is_present("print");
        cli::list_links(filter, print);
    }

    if let Some(cmd) = args.subcommand_matches("open") {
        let values: Vec<&str> = match cmd.values_of("id") {
            Some(v) => v.collect::<Vec<_>>(),
            None => vec![],
        };
        let ids: Vec<u64> = values
            .iter()
            .map(|i| match u64::from_str(i) {
                Ok(id) => Some(id),
                Err(_) => {
                    eprintln!("Not a valid ID: {}", i);
                    None
                }
            })
            .filter(|i| i.is_some())
            .map(|i| i.unwrap())
            .collect();
        cli::open_link(ids);
    }

    if let Some(cmd) = args.subcommand_matches("save") {
        let url: &str = cmd.value_of("url").unwrap();
        let annotation: Option<&str> = cmd.value_of("annotation");
        cli::save_link(url, annotation);
    }

    if let Some(cmd) = args.subcommand_matches("remove") {
        let id: u64 = match u64::from_str(cmd.value_of("id").unwrap()) {
            Ok(id) => id,
            Err(error) => panic!("Not a valid ID: {:?}", error),
        };
        cli::remove_link(id);
    }
}
