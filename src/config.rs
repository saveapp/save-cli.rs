use std::env;
use std::fs::read_to_string;

use serde::{Deserialize, Serialize};

use crate::util::SaveError;

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Config {
    api_url: String,
    username: String,
    #[serde(default = "Config::default_password")]
    password: Option<String>,
    open_command: Option<OpenCommand>,
}

impl Config {
    #[cfg(test)]
    pub fn new(
        api_url: &str,
        username: &str,
        password: Option<&str>,
        open_command: Option<OpenCommand>,
    ) -> Self {
        let api_url = String::from(api_url);
        let username = String::from(username);
        let password = match password {
            Some(p) => Some(String::from(p)),
            None => None,
        };
        Config {
            api_url,
            username,
            password,
            open_command,
        }
    }

    pub fn from_file(path: Option<&str>) -> Result<Self, SaveError> {
        let config_file_path: String = match path {
            Some(p) => String::from(p),
            None => {
                let homedir = env::var("HOME")?;
                format!("{}/.config/saveapp/cli.yml", homedir)
            }
        };
        let config_yaml = read_to_string(&config_file_path)?;
        Ok(serde_yaml::from_str(&config_yaml)?)
    }

    pub fn api_url(&self) -> &str {
        &self.api_url
    }

    pub fn username(&self) -> &str {
        &self.username
    }

    pub fn password(&self) -> &Option<String> {
        &self.password
    }

    pub fn open_command(&self) -> &Option<OpenCommand> {
        &self.open_command
    }

    fn default_password() -> Option<String> {
        match env::var("SAVE_PASSWORD") {
            Ok(pass) => return Some(pass),
            Err(_) => {
                eprintln!("No password found in config or environment");
                return None;
            }
        };
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct OpenCommand {
    command: String,
    args: Option<Vec<String>>,
}
impl OpenCommand {
    pub fn command(&self) -> &String {
        &self.command
    }

    pub fn args(&self) -> Option<Vec<String>> {
        match &self.args {
            Some(a) => Some(a.iter().map(|arg| arg.to_string()).collect()),
            None => None,
        }
    }
}

impl OpenCommand {
    pub fn new(command: &str, args: Option<Vec<&str>>) -> Self {
        let command = String::from(command);
        let args: Option<Vec<String>> = match args {
            Some(v) => Some(v.iter().map(|s| s.to_string()).collect()),
            None => None,
        };
        OpenCommand { command, args }
    }
}

#[cfg(test)]
mod test_config_from_file {
    use std::fs::{remove_file, write};

    use super::*;

    #[test]
    fn should_read_valid_config() {
        // prepare config file
        write(
            "/tmp/saveapp-test_config_from_file-valid",
            r#"---
username: foo
password: bar
api_url: https://foo.bar
open_command:
  command: firefox
  args:
  - -P
  - foo"#,
        )
        .unwrap();

        // test stuff
        let config = Config::from_file(Some("/tmp/saveapp-test_config_from_file-valid")).unwrap();
        let expected = Config::new(
            "https://foo.bar",
            "foo",
            Some("bar"),
            Some(OpenCommand::new("firefox", Some(vec!["-P", "foo"]))),
        );
        assert_eq!(config, expected);

        // cleanup
        remove_file("/tmp/saveapp-test_config_from_file-valid").unwrap();
    }

    #[test]
    fn should_look_for_password_in_env_when_missing_in_file() {
        // prepare config file
        write(
            "/tmp/saveapp-test_config_pass_from_env",
            r#"---
username: foo
api_url: https://foo.bar"#,
        )
        .unwrap();
        env::set_var("SAVE_PASSWORD", "bar");

        // test stuff
        let config = Config::from_file(Some("/tmp/saveapp-test_config_pass_from_env")).unwrap();
        let expected = Config::new("https://foo.bar", "foo", Some("bar"), None);
        assert_eq!(config, expected);

        // cleanup
        remove_file("/tmp/saveapp-test_config_pass_from_env").unwrap();
    }

    #[test]
    fn should_support_missing_open_command() {
        // prepare config file
        write(
            "/tmp/saveapp-test_config_missing_opencommand",
            r#"---
username: foo
password: bar
api_url: https://foo.bar"#,
        )
        .unwrap();

        // test stuff
        let config =
            Config::from_file(Some("/tmp/saveapp-test_config_missing_opencommand")).unwrap();
        let expected = Config::new("https://foo.bar", "foo", Some("bar"), None);
        assert_eq!(config, expected);

        // cleanup
        remove_file("/tmp/saveapp-test_config_missing_opencommand").unwrap();
    }

    #[test]
    fn should_return_not_found_error_for_nonexisting_file() {
        let config_file = "/tmp/saveapp/nonexisting-config.yml";
        let config = Config::from_file(Some(config_file));
        assert!(config.is_err());
    }

    #[test]
    fn should_return_malformatted_error_for_malformatted_yaml() {
        // prepare config file
        write("/tmp/saveapp-test_config_from_file-invalid", "invalid yaml").unwrap();

        // test stuff
        let result = Config::from_file(Some("/tmp/saveapp-test_config_from_file-invalid"));
        match result {
            Err(SaveError::ConfigMalformatted(_)) => {}
            _ => panic!(
                "Should return SaveError::ConfigMalformatted, got {:?}",
                result
            ),
        }

        // cleanup
        remove_file("/tmp/saveapp-test_config_from_file-invalid").unwrap();
    }
}
