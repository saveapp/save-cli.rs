# save -- cli
A Rust based command line interface used for saving links to read them later.

## Building and Running
Build the binary by running
```shell
cargo build --release
```

You can then run the binary `target/release/save`.

While developing you may want to build and run:
```shell
cargo run
```

## Usage
Before being able to use the saveapp CLI, you'll have to setup your config file in `~/.config/saveapp/cli.yml` to look like this:
```yaml
api_url: https://save.yourdomain.com
username: you
password: sup3rs3cure
open_command:
  command: firefox
  args:
    - -P
    - foo
```
You'll probably also want to `chmod` the file so only you can view your password.

Alternatively you can leave the password out of the file and set it via the `SAVE_PASSWORD` environment variable.
You can then run the CLI like this:
```shell
SAVE_PASSWORD=sup3rs3cure save <...>
```

Also you can leave out the `open_command` to fall back to your system's default webbrowser.

To see how to use the CLI, see the usage instructions.
You can view them by running
```shell
save --help
```
